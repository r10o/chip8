# Target
TARGET		:= chip8
TYPE		:= bin

# Version
VERSION_MAJOR	:= 0
VERSION_MINOR	:= 2
VERSION_REV	:= 0
VERSION		:= $(VERSION_MAJOR).$(VERSION_MINOR).$(VERSION_REV)

# Files
SRC := $(wildcard src/*.c)
OBJ := $(SRC:.c=.o)

# Libraries
LIBS		:= $(shell sdl-config --libs)
LIBS		+= -lSDL_mixer

# Name of output file
OUTPUT		:= $(TARGET)
LIB_LN		:=
LIB_LN_V	:=

# CFLAGS
CFLAGS		:= -Wall 
CFLAGS		+= -pipe
CFLAGS		+= -std=c99
CFLAGS		+= $(shell sdl-config --cflags)

# Defines
CFLAGS		+= -DVERSION=\"$(VERSION)\"

# LDFLAGS
LDFLAGS		:= $(LIBS)

# You can change the following depending on your system

# Options
DEBUG		:= no
PEDANTIC	:= no
OPTIMIZE 	:= yes

# Compiler and linker
CC		:= clang

# Paths
PREFIX		:= /usr/local
BIN_DIR		:= bin
DOC_DIR		:= share/$(TARGET)
INC_DIR		:= include
LIB_DIR		:= lib
MAN_DIR		:= share/man
SRC_DIR		:= src
